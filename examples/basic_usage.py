import matplotlib.pyplot as plt
import numpy as np
#
import dunescape as ds

shape = (256, 128)
seed = 1

nlevel = 5 # Barchan dunes
nlevel = 50 # "more sand": transverse ridge dunes 

w = np.random.default_rng(seed).random(shape)
w = np.round(nlevel * w)

dfield = ds.DuneField(w)

plt.ion()
fig = plt.figure()
im1 = plt.imshow(dfield.z.T, cmap='jet', origin='lower')
fig.canvas.flush_events()
fig.canvas.draw()

#
for it in range(100):
    dfield.cycle()
    
    if np.mod(it, 1) == 0:
        im1.set_data(dfield.z.T)
        im1.set_clim(0, dfield.z.max())
        fig.canvas.flush_events()
        fig.canvas.draw()
        fig.suptitle(str(it).zfill(4))
        
dfield.save('out.png')
           
plt.ioff()
plt.show()
