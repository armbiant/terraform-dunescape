import logging


class Formatter(logging.Formatter):

    def format(self, record):
        if record.levelno == logging.INFO:
            self._style._fmt = "%(message)s"
            color = ''
        else:
            color = {
                logging.WARNING: 33,
                logging.ERROR: 31,
                logging.FATAL: 31,
                logging.DEBUG: 36
            }.get(record.levelno, 0)

        self._style._fmt = f"%(name)-{0}s:%(lineno)-{0}s - %(funcName)-{0}s - \033[{color}m%(levelname)-7s\033[0m : %(message)s".format(
            len(record.name) + 5)

        return super().format(record)


def create(name):
    level = logging.DEBUG

    # Recuperation d'un logger
    logger = logging.getLogger(name)
    logger.setLevel(level)

    # Suppression de tous les handlers
    for hdl in logger.handlers:
        logger.removeHandler(hdl)

    # Format d'affichage
    format = u"%(name)-{0}s - %(levelname)-7s : %(message)s"\
             .format(len(name)+5)
    formatter = logging.Formatter(format)

    console_log = logging.StreamHandler()
    console_log.setLevel(level)
    #console_log.setFormatter(formatter)
    console_log.setFormatter(Formatter())

    # Activation de l'affichage console
    logger.addHandler(console_log)

    return logger
