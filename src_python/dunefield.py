import cv2
import matplotlib.pyplot as plt
import numpy as np
import os
import random
#
import dunescape_core
import dunescape.logger as logger

log = logger.create(os.path.basename(__file__))
eps = 1e-5


class DuneField:

    def __init__(self,
                 z,
                 mask_static=None,
                 hop_length=5,
                 slab_height=1,
                 shadow_slope=3 * np.tan(15 / 180 * np.pi),
                 prob_deposit_bare=0.4,
                 prob_deposit_sand=0.6,
                 av_method='Moore down',
                 seed=None):
        """
        Dune field constructor.

        Parameters
        ----------
        z : ndarray
            Dune field heightmap, based on integer values (the height is
            based on a number of cells).
        mask_static : ndarray, optional
            Mask defining static nodes. Default is None.
        hop_length : float, optional
            Hop length. Default is 5.
        slab_height : float, optional
            Slab height. Default is 5.
        shadow_slope : float, optional
            Shadow slope. Default is 3 * np.tan(15 / 180 * np.pi).
        prob_deposit_bare : float, optional
            Probability of deposition on bare ground. Default is 0.4.
        prob_deposit_sand : float, optional
            Probability of deposition on sandy ground. Default is 0.6.
        av_method : str, optional
            Avalanching search pattern. Default is 'Moore down'.
        seed : int, optional
            Random number seed. Default is None, no seeding.
        """

        log.debug('New instance...')

        if not (seed is None):
            random.seed(seed)

        # input
        self.z = z
        self.hop_length = hop_length
        self.slab_height = slab_height
        self.shadow_slope = shadow_slope
        self.prob_deposit_bare = prob_deposit_bare
        self.prob_deposit_sand = prob_deposit_sand

        if av_method == 'Moore down':
            self.av_method = 1
        elif av_method == 'Moore classic':
            self.av_method = 0

        # computed
        self.shape = self.z.shape
        self.shadow = np.zeros(self.shape)

        if mask_static is not None:
            self.mask_static = mask_static
        else:
            self.mask_static = np.zeros(self.shape)

        log.debug('Initializing fortran module...')

        dunescape_core.param.init(self.mask_static, self.slab_height,
                                  self.shadow_slope, self.av_method,
                                  self.hop_length, self.prob_deposit_bare,
                                  self.prob_deposit_sand)

        return None

    def cycle(self, ilist=[], jlist=[]):
        """
        Perform on cycle of the cellular automata.

        Parameters
        ----------
        ilist : list of int, optional
            List of i indices of cells to update. By default the cells
            are randomly chosen.
        jlist : list of int, optional
            List of i indices of cells to update. By default the cells
            are randomly chosen.
        """

        if not (len(ilist)) or not (len(jlist)):
            ijnsh = np.where(self.shadow > 0)
            ijnz = np.where(self.z > 0)
            ilist = np.append(ijnsh[0], ijnz[0])
            jlist = np.append(ijnsh[1], ijnz[1])

            rlist = [r for r in range(len(ilist))]
            random.shuffle(rlist)
            ilist = ilist[rlist]
            jlist = jlist[rlist]

        self.z, self.shadow = dunescape_core.cycle(self.z, self.shadow, ilist,
                                                   jlist)
        return None

    def save(self, fname):
        """
        Save heightmap to an image file (and also a '.raw' file).

        Parameters
        ----------
        fname : str
            Filename.
        """

        zn = (self.z - self.z.min()) / self.z.ptp()

        log.debug('writing file: {}'.format(fname))
        zn = np.array(zn * 255).astype('uint8')
        cv2.imwrite(fname, zn.T)

        fname_raw = fname + '.raw'
        log.debug('writing raw file: {}'.format(fname_raw))
        zi = (65535 * zn.astype(np.uint16))
        with open(fname_raw, 'wb') as f:
            zi.T.tofile(f)

        return None
