module param
   implicit none
   real(8) :: p_slab_height
   real(8) :: p_shadow_slope
   integer :: av_method ! 0:moore, 1:moore_down
   integer :: p_hop_length
   real(8) :: p_prob_deposit_bare
   real(8) :: p_prob_deposit_sand
   !
   real(8) :: eps = 1e-10
   integer, allocatable, dimension(:) :: isu, jsu, isd, jsd
   integer, allocatable, dimension(:, :) :: p_mask_static
contains

   subroutine init(mask_static, slab_height, shadow_slope, av_method,&
        & hop_length, prob_deposit_bare, prob_deposit_sand, nx, ny)
      implicit none
      real(8), intent(in) :: slab_height
      real(8), intent(in) :: shadow_slope
      integer, intent(in) :: av_method
      integer, intent(in) :: hop_length
      real(8), intent(in) :: prob_deposit_bare
      real(8), intent(in) :: prob_deposit_sand
      integer, intent(in) :: nx, ny
      integer, intent(in), dimension(0:nx - 1, 0:ny - 1) :: mask_static

      p_slab_height = slab_height
      p_shadow_slope = shadow_slope
      p_hop_length = hop_length
      p_prob_deposit_bare = prob_deposit_bare
      p_prob_deposit_sand = prob_deposit_sand
      allocate (p_mask_static(0:nx - 1, 0:ny - 1))
      p_mask_static = mask_static

      if (av_method == 0) then ! Moore classic
         allocate (isu(8), jsu(8), isd(8), jsd(8))
         ! 6 2 8
         ! 1 . 4
         ! 5 3 7
         isu = (/-1, 0, 0, 1, -1, -1, 1, 1/)
         jsu = (/0, 1, -1, 0, -1, 1, -1, 1/)

         ! 8 2 6
         ! 4 . 1
         ! 7 3 5
         isd = (/1, 0, 0, -1, 1, 1, -1, -1/)
         jsd = (/0, 1, -1, 0, -1, 1, -1, 1/)

      else if (av_method == 1) then ! Moore down
         ! 5 2 -
         ! 1 . -
         ! 4 3 -
         allocate (isu(5), jsu(5), isd(5), jsd(5))
         isu = (/-1, 0, 0, -1, -1/)
         jsu = (/0, 1, -1, -1, 1/)

         ! - 2 5
         ! - . 1
         ! - 3 4
         isd = (/1, 0, 0, 1, 1/)
         jsd = (/0, 1, -1, -1, 1/)

      end if

   end subroutine init

end module param

! --------------------- CYCLE

subroutine cycle(z, shadow, ilist, jlist, nx, ny, npt)
   use param
   implicit none
   ! inputs
   integer, intent(in) :: nx, ny, npt
   integer, intent(in), dimension(0:npt - 1) :: ilist, jlist
   real(8), intent(inout), dimension(0:nx - 1, 0:ny - 1) :: z
   integer, intent(inout), dimension(0:nx - 1, 0:ny - 1) :: shadow
   !
   !f2py intent(in,out) :: z
   !f2py intent(in,out) :: shadow
   !
   ! locals
   integer :: i, j, k, iu
   real(8) :: rd

   call init_shadow(z, shadow, nx, ny)

   do k = 0, npt - 1
      i = ilist(k)
      j = jlist(k)

      ! if bare cell, don't do anything
      if ((z(i, j) < eps) .or. (shadow(i, j) == 1) .or. (p_mask_static(i, j) == 1)) then
         !

      else
         ! remove slab from initial cell
         call erode_cell(z, shadow, i, j, nx, ny)

         do
            ! move to downwind cell (w/ periodic domain)
            i = modulo(i + p_hop_length, nx)

            if (p_mask_static(i, j) > 0) then
               ! find 1st upstream non-static cell and deposit sand
               ! there
               do iu = i - 1, 0, -1
                  if (p_mask_static(iu, j) == 0) then
                     exit
                  end if
               end do
               call deposit_cell(z, shadow, iu, j, nx, ny)
               exit

            else if (shadow(i, j) == 1) then
               call deposit_cell(z, shadow, i, j, nx, ny)
               exit

            else
               call random_number(rd)
               if (((z(i, j) .lt. eps) .and. (rd .lt. p_prob_deposit_bare)) &
                   .or. (rd .lt. p_prob_deposit_sand)) then
                  call deposit_cell(z, shadow, i, j, nx, ny)
                  exit
               end if
            end if
         end do
      end if
   end do

end subroutine cycle

! --------------------- SHADOW

subroutine init_shadow(z, shadow, nx, ny)
   use param
   implicit none
   integer, intent(in) :: nx, ny
   real(8), intent(in), dimension(0:nx - 1, 0:ny - 1) :: z
   integer, intent(inout), dimension(0:nx - 1, 0:ny - 1) :: shadow
   !
   integer :: ir, i, j
   real(8), dimension(0:nx - 1) :: slope
   real(8), dimension(0:nx - 1, 0:ny - 1) :: zs, zroll

   shadow(:, :) = 0
   zs(:, :) = 0.0

   do i = 0, nx - 1
      slope(i) = real(i)*p_shadow_slope
   end do

   do ir = 0, nx - 1
      zroll(:, :) = cshift(z(:, :), ir + 1, dim=1)
      do i = 0, nx - 1
         zs(i, :) = zroll(nx - 1 - i, :) - z(ir, :) - slope(i)
      end do
      do j = 0, ny - 1
         shadow(ir, j) = int(max(maxval(zs(:, j))/abs(maxval(zs(:, j))), 0.0))
      end do
   end do

end subroutine init_shadow

subroutine update_shadow(z, shadow, i, j, nx, ny)
   use param
   implicit none
   integer, intent(in) :: nx, ny, i, j
   real(8), intent(in), dimension(0:nx - 1, 0:ny - 1) :: z
   integer, intent(inout), dimension(0:nx - 1, 0:ny - 1) :: shadow
   !
   integer :: ir, irm, k, dmax
   real(8), dimension(0:nx - 1) :: slope
   real(8), dimension(0:nx - 1) :: zs, zroll

   zs(:) = 0.0

   do k = 0, nx - 1
      slope(k) = real(k)*p_shadow_slope
   end do

   dmax = int(z(i, j)) + 2

   do ir = i - dmax, i + dmax
      irm = modulo(ir, nx)
      zroll(:) = cshift(z(:, j), irm + 1, dim=1)
      do k = 0, nx - 1
         zs(k) = zroll(nx - 1 - k) - z(irm, j) - slope(k)
      end do
      shadow(irm, j) = int(max(maxval(zs(:))/abs(maxval(zs(:))), 0.0))
   end do

end subroutine update_shadow

subroutine global_avalanching(z, shadow, nx, ny)
   use param
   implicit none
   integer, intent(in) :: nx, ny
   real(8), intent(inout), dimension(0:nx - 1, 0:ny - 1) :: z
   integer, intent(inout), dimension(0:nx - 1, 0:ny - 1) :: shadow
   !
   integer :: i, j, id, jd

   do j = 0, ny - 1
      do i = 0, nx - 1
         call find_dw(z, i, j, id, jd, nx, ny)
         z(i, j) = z(i, j) - p_slab_height
         z(id, jd) = z(id, jd) + p_slab_height
      end do
   end do

   call init_shadow(z, shadow, nx, ny)
end subroutine global_avalanching

! --------------------- MISC.

subroutine erode_cell(z, shadow, i, j, nx, ny)
   use param
   implicit none
   integer, intent(in) :: nx, ny, i, j
   real(8), intent(inout), dimension(0:nx - 1, 0:ny - 1) :: z
   integer, intent(inout), dimension(0:nx - 1, 0:ny - 1) :: shadow
   !
   integer :: iu, ju

   call find_up(z, i, j, iu, ju, nx, ny)
   z(iu, ju) = z(iu, ju) - p_slab_height
   call update_shadow(z, shadow, iu, ju, nx, ny)
end subroutine erode_cell

subroutine deposit_cell(z, shadow, i, j, nx, ny)
   use param
   implicit none
   integer, intent(in) :: nx, ny, i, j
   real(8), intent(inout), dimension(0:nx - 1, 0:ny - 1) :: z
   integer, intent(inout), dimension(0:nx - 1, 0:ny - 1) :: shadow
   !
   integer :: id, jd

   call find_dw(z, i, j, id, jd, nx, ny)
   z(id, jd) = z(id, jd) + p_slab_height
   call update_shadow(z, shadow, id, jd, nx, ny)
end subroutine deposit_cell

subroutine find_up(z, i, j, iu, ju, nx, ny)
   use param
   implicit none
   integer, intent(in) :: nx, ny, i, j
   integer, intent(inout) :: iu, ju
   real(8), intent(inout), dimension(0:nx - 1, 0:ny - 1) :: z
   !
   integer :: k, ik, jk

   iu = i
   ju = j

   do k = 1, size(isu)
      ik = modulo(i + isu(k), nx)
      jk = modulo(j + jsu(k), ny)

      if (((z(ik, jk) - z(i, j)) .ge. 2.0*p_slab_height - eps) .and. (p_mask_static(ik, jk) == 0)) then
         iu = ik
         ju = jk
         exit
      end if
   end do

end subroutine find_up

subroutine find_dw(z, i, j, id, jd, nx, ny)
   use param
   implicit none
   integer, intent(in) :: nx, ny, i, j
   integer, intent(inout) :: id, jd
   real(8), intent(inout), dimension(0:nx - 1, 0:ny - 1) :: z
   !
   integer :: k, ik, jk

   id = i
   jd = j

   do k = 1, size(isd)
      ik = modulo(i + isd(k), nx)
      jk = modulo(j + jsd(k), ny)

      if (((z(i, j) - z(ik, jk)) .ge. 2.0*p_slab_height - eps) .and. (p_mask_static(ik, jk) == 0)) then
         id = ik
         jd = jk
         exit
      end if
   end do

end subroutine find_dw
