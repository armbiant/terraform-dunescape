<img src="pics/banner.png">

# Dunescape

Python package to generate dune fields using cellular automaton.

[toc]

## License

This project is licensed under the GNU General Public License v3.0.

## Getting started

### Installation

At some point a `PyPi` package will be able but for now this has to be done manually.

Use git to clone this repository into your computer:
```shell
git clone https://gitlab.com/procedural2/dunescape
```

The package makes use of `f2py` and therefore requires to be built before usage: 
```shell
cd dunescape
python3 setup.py build
```

Add to the `PYTHONPATH` the path to the python lib that has just been build (replace `XXX` by the proper lib extension):
```
export PYTHONPATH="${PYTHONPATH}:[BUILD PATH/lib.XXX]"
```

### Usage examples

Examples are available in the [`examples/`](examples/) folder.

- Barchan dunes
<img src="pics/ex_barchan_dunes.png" width="256">

- Transverse dunes
<img src="pics/ex_transverse_dunes.png" width="256">

